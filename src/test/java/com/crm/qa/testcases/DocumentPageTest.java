/*
 * Pritam
 */
package com.crm.qa.testcases;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.crm.qa.base.TestBase;
import com.crm.qa.pages.DocumentPage;
import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.LoginPage;
import com.crm.qa.util.TestUtil;

public class DocumentPageTest extends TestBase {
	
	LoginPage loginPage;
	HomePage homePage;
	TestUtil testUtil;
	DocumentPage documentPage;
	
	String sheetName = "documents";
	
	public DocumentPageTest() {
		super();
	}
	
	@BeforeMethod
	public void setUp() {
		initialization();
		testUtil = new TestUtil();
		loginPage = new LoginPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		testUtil.switchToFrame();
		documentPage = homePage.clickOnDocumentLink();
	}
	
	@Test(priority=1)
	public void verifyDocumentPageLabel() {
		Assert.assertTrue(documentPage.verifyDocLabel(), "doc label is missing on the page");
	}
	
	@DataProvider
	public Object[][] getCRMTestData() throws InvalidFormatException {
		Object[][] data = TestUtil.getTestData(sheetName);
		return data;
	}
	
	@Test(priority=2, dataProvider="getCRMTestData")
	public void validateCreateNewDoc(String titl, String desc, String ver, String cont, String comp, String cas) throws InterruptedException {
		homePage.clickOnNewDocumentLink();
		documentPage.createNewDocument(titl, desc, ver, cont, comp, cas);
		boolean title = documentPage.clickDocs();
		Assert.assertTrue(title, "Root Folder");
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}

