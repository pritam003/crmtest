/*
 * Pritam
 */
package com.crm.qa.testcases;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.crm.qa.base.TestBase;
import com.crm.qa.pages.CallPage;
import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.LoginPage;
import com.crm.qa.util.TestUtil;

public class CallPageTest extends TestBase {
	
	LoginPage loginPage;
	HomePage homePage;
	TestUtil testUtil;
	CallPage callPage;
	
	String sheetName = "calls";
	
	public CallPageTest() {
		super();
	}
	
	@BeforeMethod
	public void setUp() {
		initialization();
		testUtil = new TestUtil();
		loginPage = new LoginPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		testUtil.switchToFrame();
		callPage = homePage.clickOnCallLink();
	}
	
	@Test(priority=1)
	public void verifyCallPageLabel() {
		Assert.assertTrue(callPage.verifyCallLabel(), "call label is missing on the page");
	}
	
	@DataProvider
	public Object[][] getCRMTestData() throws InvalidFormatException {
		Object[][] data = TestUtil.getTestData(sheetName);
		return data;
	}
	
	@Test(priority=2, dataProvider="getCRMTestData")
	public void validateCreateNewCall(String comp1, String flagg, String close, String deal, String tasks, String casess) {
		homePage.clickOnNewCallLink();
		callPage.createNewCall(comp1, flagg, close, deal, tasks, casess);
		//boolean title = callPage.clickCalls();
		//Assert.assertTrue(title, "Call Information");
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
	
	

}
