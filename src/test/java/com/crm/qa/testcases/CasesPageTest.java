package com.crm.qa.testcases;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.crm.qa.base.TestBase;
import com.crm.qa.pages.CasesPage;
import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.LoginPage;
import com.crm.qa.util.TestUtil;

public class CasesPageTest extends TestBase {
	LoginPage loginPage;
	HomePage homePage;
	TestUtil testUtil;
	CasesPage casesPage;
	
	String sheetName = "cases";
	
	public CasesPageTest() {
		super();
	}
	
	@BeforeMethod
	public void setUp() {
		initialization();
		testUtil = new TestUtil();
		loginPage = new LoginPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		testUtil.switchToFrame();
		casesPage = homePage.clickOnCasesLink();
	}
	
	@Test(priority=1)
	public void verifyDealsPageLabel() {
		Assert.assertTrue(casesPage.verifyCasesLabel(), "cases label is missing on the page");
	}
	
	@DataProvider
	public Object[][] getCRMTestData() throws InvalidFormatException {
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	
	@Test(priority=2, dataProvider="getCRMTestData")
	public void validateCreateNewCase(String titl, String stat, String typ, String prio) {
		homePage.clickOnNewCaseLink();
		casesPage.createNewCases(titl, stat, typ, prio);
	}
	
	@Test(priority=3)
	public void validateNewCase() {
		homePage.clickOnCasesLink();
		casesPage.clickShowAll();
		Assert.assertTrue(casesPage.clickShowAll(), "Support Cases - Open and Closed Cases");
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
