package com.crm.qa.testcases;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.crm.qa.base.TestBase;
import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.LoginPage;
import com.crm.qa.pages.TasksPage;
import com.crm.qa.util.TestUtil;

public class TasksPageTest extends TestBase {
	
	LoginPage loginPage;
	HomePage homePage;
	TestUtil testUtil;
	TasksPage tasksPage;
	
    String sheetName = "tasks";
	
	public TasksPageTest() {
		super();
	}
	
	@BeforeMethod
	public void setUp() {
		initialization();
		testUtil = new TestUtil();
		loginPage = new LoginPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		testUtil.switchToFrame();
		tasksPage = homePage.clickOnTasksLink();
	}
	
	@Test(priority=1)
	public void verifyTasksPageLabel() {
		Assert.assertTrue(tasksPage.verifyTasksLabel(), "tasks label is missing on the page");
	}
	
	@DataProvider
	public Object[][] getCRMTestData() throws InvalidFormatException {
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	
	@Test(priority=2, dataProvider="getCRMTestData")
	public void validateCreateNewTask(String titl, String deals, String keycompany, String typ) {
		homePage.clickOnNewTaskLink();
		tasksPage.createNewTasks(titl, deals, keycompany, typ);
	}
	
	@Test(priority=3)
	public void validateNewTask() {
		homePage.clickOnTasksLink();
		tasksPage.clickTasks();
		boolean title = tasksPage.clickTasks();
		Assert.assertTrue(title, "Tasks - Showing all tasks");
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}

	


