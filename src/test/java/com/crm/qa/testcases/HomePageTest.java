package com.crm.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.crm.qa.base.TestBase;
import com.crm.qa.pages.CallPage;
import com.crm.qa.pages.CasesPage;
import com.crm.qa.pages.ContactsPage;
import com.crm.qa.pages.DealsPage;
import com.crm.qa.pages.DocumentPage;
import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.LoginPage;
import com.crm.qa.pages.TasksPage;
import com.crm.qa.util.TestUtil;

public class HomePageTest extends TestBase {
	
	LoginPage loginPage;
	HomePage homePage;
	TestUtil testUtil;
	ContactsPage contactsPage;
	DealsPage dealsPage;
	TasksPage tasksPage;
	CasesPage casesPage;
	CallPage  callPage;
	DocumentPage documentPage;
	
		
	public HomePageTest() {
		super();
	}
	
	//test cases should be separated -- independent with each other
	//before each test cases -- launch the browser and login
	//@Test -- execute test case
	//after each test case -- close the browser
	@BeforeMethod
	public void setUp() {
		initialization();
		testUtil = new TestUtil();
		contactsPage = new ContactsPage();
		dealsPage = new DealsPage();
		tasksPage = new TasksPage();
		casesPage = new CasesPage();
		callPage = new CallPage();
		documentPage = new DocumentPage();
		loginPage = new LoginPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
	}
	
	@Test(priority=1)
	public void homePageTilteTest() {
		String homePageTitle = homePage.verifyHomePageTitle();
		Assert.assertEquals(homePageTitle, "CRMPRO", "Home page title not matched");
	}
	
	@Test(priority=2)
	public void verifyUserNameTest() {
		testUtil.switchToFrame();
		Assert.assertTrue(homePage.verifyCorrectUserName());
	}
	
	@Test(priority=3)
	public void  verifyContactsLink() {
		testUtil.switchToFrame();
		contactsPage = homePage.clickOnContactsLink();
	}
	
	@Test(priority=4)
	public void verifyDealsLink() {
		testUtil.switchToFrame();
		dealsPage = homePage.clickOnDealsLink();
	}
	
	@Test(priority=5)
	public void verifyTasksLink() {
		testUtil.switchToFrame();
		tasksPage = homePage.clickOnTasksLink();
	}
	
	@Test(priority=6)
	public void verifyCasesLink() {
		testUtil.switchToFrame();
		casesPage = homePage.clickOnCasesLink();
	}
	
	@Test(priority=7)
	public void verifyCallLink() {
		testUtil.switchToFrame();
		callPage = homePage.clickOnCallLink();
	}
 	
	@Test(priority=8)
	public void verifyDocumentLink() {
		testUtil.switchToFrame();
		documentPage = homePage.clickOnDocumentLink();
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
