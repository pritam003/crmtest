/*
 * Pritam
 */

package com.crm.qa.testcases;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.crm.qa.base.TestBase;
import com.crm.qa.pages.DealsPage;
import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.LoginPage;
import com.crm.qa.util.TestUtil;

public class DealsPageTest extends TestBase {
	
	LoginPage loginPage;
	HomePage homePage;
	TestUtil testUtil;
	DealsPage dealsPage;
	
	String sheetName = "deals";
	
	public DealsPageTest() {
		super();
	}
	
	@BeforeMethod
	public void setUp() {
		initialization();
		testUtil = new TestUtil();
		loginPage = new LoginPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		testUtil.switchToFrame();
		dealsPage = homePage.clickOnDealsLink();
	}

	@Test(priority=1)
	public void verifyDealsPageLabel() {
		Assert.assertTrue(dealsPage.verifyDealsLabel(), "deals label is missing on the page");
	}
	
	@DataProvider
	public Object[][] getCRMTestData() throws InvalidFormatException {
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	
	@Test(priority=2, dataProvider="getCRMTestData")
	public void validateCreateNewDeal(String titl, String comp, String primarycontact, String typ) {
		homePage.clickOnNewDealLink();
		dealsPage.createNewDeals(titl, comp, primarycontact, typ);
	}
	
	@Test(priority=3)
	public void validateNewDeal() {
		homePage.clickOnDealsLink();
		dealsPage.clickShowAll();
		boolean title = dealsPage.clickShowAll();
		Assert.assertTrue(title, "Deals");
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
