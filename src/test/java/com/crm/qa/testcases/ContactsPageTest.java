/*
 * @author Pritam Kumar Routray
 * 
 */

package com.crm.qa.testcases;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.crm.qa.base.TestBase;
import com.crm.qa.pages.ContactsPage;
import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.LoginPage;
import com.crm.qa.util.TestUtil;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ContactsPageTest extends TestBase {
	
	LoginPage loginPage;
	HomePage homePage;
	TestUtil testUtil;
	ContactsPage contactsPage;
	
	public ExtentReports extent;
	public ExtentTest extentTest;
	
	String sheetName = "contacts";
	
	public ContactsPageTest() {
		super();
	}
	
	@BeforeTest
	public void setExtent() {
		extent = new ExtentReports(System.getProperty("user.dir")+"/test-output/ExtentReport.html", true);
		extent.addSystemInfo("Host Name", "Pritam Automation");
		extent.addSystemInfo("User Name", "Pritam Automation Labs");
		extent.addSystemInfo("Environment", "QA");
		extent.addSystemInfo("Java Version", "1.8.0_25");
		extent.addSystemInfo("OS", "Windows 8.1");
	}
	
	@AfterTest
	public void endReport() {
		extent.flush();
		extent.close();
	}
	
	public static String getScreenShot(WebDriver driver, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destination = System.getProperty("user.dir")+ "/FailedTestsScreenshots/" + screenshotName + dateName 
				+ ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}
 		 
	@BeforeMethod
	public void setUp() {
		initialization();
		testUtil = new TestUtil();
		contactsPage = new ContactsPage();
		loginPage = new LoginPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		testUtil.switchToFrame();
		contactsPage = homePage.clickOnContactsLink();
	}
	
	@Test(priority=1)
	public void verifyContactsPageLabel() {
		extentTest = extent.startTest("verifyContactsPageLabel");
		Assert.assertTrue(contactsPage.verifyContactsLabel(), "contacts label is missing on the page");
	}
	
	@Test(priority=2)
	public void selectSingleContactsTest() {
		extentTest = extent.startTest("selectSingleContactsTest");
		contactsPage.selectContactsByName("Test1 Test");
	}
	
	@Test(priority=3)
	public void selectMultipleContactsTest() {
		extentTest = extent.startTest("selectMultipleContactsTest");
		contactsPage.selectContactsByName("Test1 Test");
		contactsPage.selectContactsByName("ui uii");
	}
	
	@DataProvider
	public Object[][] getCRMTestData() throws InvalidFormatException {
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	
	@Test(priority=4, dataProvider="getCRMTestData")
	public void validateCreateNewContact(String title, String firstName, String lastName, String company) {
		extentTest = extent.startTest("validateCreateNewContact");
		homePage.clickOnNewContactLink();
		//contactsPage.createNewContact("Mr.", "Tom", "Peter", "Google");
		contactsPage.createNewContact(title, firstName, lastName, company);
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if(result.getStatus()==ITestResult.FAILURE) {
			extentTest.log(LogStatus.FAIL, "TEST CASE FAILED IS " +result.getName()); //To add name in extent Report
			extentTest.log(LogStatus.FAIL, "TEST CASE FAILED IS " +result.getThrowable()); //To add error/exception in extent Report
			
			String screenshotPath = ContactsPageTest.getScreenShot(driver, result.getName()); 
			extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(screenshotPath)); //To add screenshot in extent Report 
			//extentTest.log(LogStatus.FAIL, extentTest.addScreencast(screenshotPath)); //To add video in extent Report
		}
		else if(result.getStatus()== ITestResult.SKIP) {
			extentTest.log(LogStatus.SKIP, "Test Case SKIPPED IS " +result.getName());
		}
		else if(result.getStatus()== ITestResult.SUCCESS) {
			extentTest.log(LogStatus.PASS, "Test Case PASSED IS " +result.getName());
		}
		
		extent.endTest(extentTest); //ending test and ends the current test and prepare to create a HTML Report
		driver.quit();
	}
}
