package com.crm.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.crm.qa.base.TestBase;

public class HomePage extends TestBase {
	
	//Page Factory - OR:
	@FindBy(xpath="//td[contains(text(),'User: Pritam Kumar Routray')]")
	@CacheLookup
	WebElement userNameLabel;
	
	@FindBy(xpath="//a[contains(text(),'Contacts')]")
	WebElement contactsLink;
	 
	 @FindBy(xpath="//a[contains(text(),'New Contact')]")
	 WebElement newContactLink;
	 
	 @FindBy(xpath="//a[contains(text(),'Deals')]")
	 WebElement dealsLink;
	 
	 @FindBy(xpath="//a[contains(text(),'New Deal')]")
	 WebElement newDealLink;
	 
	 @FindBy(xpath="//a[contains(text(),'Tasks')]")
	 WebElement tasksLink;
	 
	 @FindBy(xpath="//a[contains(text(),'New Task')]")
	 WebElement newTaskLink;
	 
	 @FindBy(xpath="//a[contains(text(),'Cases')]")
	 WebElement casesLink;
	 
	 @FindBy(xpath="//a[contains(text(),'New Case')]")
	 WebElement newCaseLink;
	 
	 @FindBy(xpath="//a[contains(text(),'Call')]")
	 WebElement callLink;
	 
	 @FindBy(xpath="//a[contains(text(),'New Call')]")
	 WebElement newCallLink;
	 
	 @FindBy(xpath="//a[contains(text(),'Docs')]")
	 WebElement docsLink;
	 
	 @FindBy(xpath="//a[contains(text(),'New Document')]")
	 WebElement newDocLink;
	 
	 
	 //Initializing the Page Objects:
	 public HomePage() {
		 PageFactory.initElements(driver, this);
	 }
	 
	//Actions:
	 public String verifyHomePageTitle() {
		 return driver.getTitle();
	 }
	 
	 public boolean verifyCorrectUserName() {
		 return userNameLabel.isDisplayed();
	 }
	 
	 public ContactsPage clickOnContactsLink() {
		 contactsLink.click();
		 return new ContactsPage();
	 }
	 
	 public DealsPage clickOnDealsLink() {
		 dealsLink.click();
		 return new DealsPage();
	 }
	 
	 public TasksPage clickOnTasksLink() {
		 tasksLink.click();
		 return new TasksPage();
	 }
	 
	 public CasesPage clickOnCasesLink() {
		 casesLink.click();
		 return new CasesPage();
	 }
	 
	 public CallPage clickOnCallLink() {
		 callLink.click();
		 return new CallPage();
	 }
	 
	 public DocumentPage clickOnDocumentLink() {
		 docsLink.click();
		 return new DocumentPage();
	 }
	 
	 public void clickOnNewContactLink() {
		 Actions action = new Actions(driver);
		 action.moveToElement(contactsLink).build().perform();
		 newContactLink.click();
	}
	 
	public void clickOnNewDealLink() {
		Actions action = new Actions(driver);
		action.moveToElement(dealsLink).build().perform();
		newDealLink.click();
	}
	
	public void clickOnNewTaskLink() {
		Actions action = new Actions(driver);
		action.moveToElement(tasksLink).build().perform();
		newTaskLink.click();
	}
	
	public void clickOnNewCaseLink() {
		Actions action = new Actions(driver);
		action.moveToElement(casesLink).build().perform();
		newCaseLink.click();
	}
	
	public void clickOnNewCallLink() {
		Actions action = new Actions(driver);
		action.moveToElement(callLink).build().perform();
		newCallLink.click();
	}
	
	public void clickOnNewDocumentLink() {
		Actions actions = new Actions(driver);
		actions.moveToElement(docsLink).build().perform();
		newDocLink.click();
	}
}
