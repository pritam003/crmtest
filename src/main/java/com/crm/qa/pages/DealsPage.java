package com.crm.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.crm.qa.base.TestBase;

public class DealsPage extends TestBase {
	
	//Page Factory - OR:
    @FindBy(xpath="//td[contains(text(),'Deals')]")
	WebElement dealsLabel;
    
    @FindBy(name="title")
    WebElement title;
    
    @FindBy(name="client_lookup")
	WebElement company;
    
    @FindBy(name="contact_lookup")
	WebElement primaryContact;
    
    @FindBy(name="type")
	WebElement type;
		
	@FindBy(xpath="//input[@type='submit' and @value='Save']")
	WebElement saveBtn;
	
	@FindBy(xpath="//input[@type='button' and @value='Show All']")
	WebElement showAll;
	
	@FindBy(xpath="//td[contains(text(),'Deals')]")
	WebElement deals;
	
	//Initializing the Page Objects
	public DealsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public boolean verifyDealsLabel() {
		return dealsLabel.isDisplayed();
	}
	
	public void createNewDeals(String titl, String comp, String primarycontact, String typ) {
		title.sendKeys(titl);
		company.sendKeys(comp);
		primaryContact.sendKeys(primarycontact);
		type.sendKeys(typ);
		saveBtn.click();
	}
	
	public boolean clickShowAll() {
		showAll.click();
		return deals.isDisplayed();
	}
}
