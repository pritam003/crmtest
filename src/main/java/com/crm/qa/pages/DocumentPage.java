package com.crm.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.crm.qa.base.TestBase;

public class DocumentPage extends TestBase {
	
	//Page Factory - OR:
	@FindBy(xpath="//a[text()='Docs']")
	WebElement docsLabel;
	
	@FindBy(name="title")
    WebElement title;
	
	@FindBy(name="description")
    WebElement description;
	
	@FindBy(name="version")
    WebElement version;
	
	@FindBy(xpath = "//input[@type='file' and @name = 'file' and @class= 'select']")
	WebElement fileUpload;
	
	@FindBy(xpath= "//input[@type='checkbox' and @name = 'overwrite' and @class= 'select']")
	WebElement overwrite;
	
	@FindBy(name="contact_lookup")
	WebElement contact;
	
	@FindBy(name="client_lookup")
	WebElement company;
	
	@FindBy(name="case_lookup")
	WebElement cases;
	
	@FindBy(xpath="//td[contains(text(),'Root Folder')]")
	WebElement rootFolder;
	
	@FindBy(xpath= "//input[@type='submit' and @value='Save' and @class = 'button']//parent::td[@align='left']")
	WebElement saveBtn;
	
	//Initializing the Page Object
    public DocumentPage() {
		PageFactory.initElements(driver, this);
	}
	
	public boolean verifyDocLabel() {
		return docsLabel.isDisplayed();
	}
	
	public void createNewDocument(String titl, String desc, String ver, String cont, String comp, String cas) throws InterruptedException {
		title.sendKeys(titl);
		description.sendKeys(desc);
		version.sendKeys(ver);
		fileUpload.click();
		//Runtime.getRuntime().exec(command);
		overwrite.click();
		contact.sendKeys(cont);
		company.sendKeys(comp);
		cases.sendKeys(cas);
		saveBtn.click();
	}
	
	public boolean clickDocs() {
		docsLabel.click();
		return rootFolder.isDisplayed();
	}

}
