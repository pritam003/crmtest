package com.crm.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.crm.qa.base.TestBase;

public class CasesPage extends TestBase {
	
	//Page Factory - OR:
    @FindBy(xpath="//td[contains(text(),'Cases')]")
	WebElement casesLabel;
    
    @FindBy(name="title")
    WebElement title;
    
    @FindBy(name="status")
    WebElement status;
    
    @FindBy(name="type")
    WebElement type;
	
    @FindBy(name="priority")
    WebElement priority;
    
    @FindBy(xpath="//input[@type='submit' and @value='Save']")
	WebElement saveBtn;
    
    @FindBy(xpath="//input[@type='button' and @value='Show All']")
	WebElement showAll;
    
    @FindBy(xpath="//td[contains(text(),'Support Cases - Open Cases')]")
	WebElement cases;
    
  //Initializing the Page Object
    public CasesPage() {
		PageFactory.initElements(driver, this);
	}
    
    public boolean verifyCasesLabel() {
		return casesLabel.isDisplayed();
	}
    
    public void createNewCases(String titl, String stat, String typ, String prio) {
		title.sendKeys(titl);
		status.sendKeys(stat);
		type.sendKeys(typ);
		priority.sendKeys(prio);
		saveBtn.click();
	}
    
    public boolean clickShowAll() {
    	casesLabel.click();
    	showAll.click();
    	return cases.isDisplayed();
		
	}


}
