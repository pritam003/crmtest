package com.crm.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.crm.qa.base.TestBase;

public class CallPage extends TestBase {
	
	//Page Factory - OR:
	@FindBy(xpath="//a[text()='Call']")
	WebElement callLabel;
	
	@FindBy(xpath="//input[@type='text' and @name='client_lookup']")
	WebElement company;
	
	@FindBy(name="flag")
	WebElement flag;
	
	@FindBy(name="closed")
	WebElement closed;
	
	@FindBy(xpath="//input[@type='text' and @name='prospect_lookup']")
	WebElement deal;
	
	@FindBy(xpath="//input[@type='text' and @name='task_lookup']")
	WebElement task;
	
	@FindBy(xpath="//input[@type='text' and @name='case_lookup']")
	WebElement cases;
	
	@FindBy(xpath="//input[@type='submit' and @value='Save' and @name='save']//parent::td[@align='left']")
	WebElement saveBtn;
	
	@FindBy(xpath="//td[contains,'Call Information']")
	WebElement callInfo;
	   
	//Initializing the Page Object
    public CallPage() {
		PageFactory.initElements(driver, this);
	}
    
    public boolean verifyCallLabel() {
    	return callLabel.isDisplayed();
    }
    
    public void createNewCall(String comp, String flags, String close, String deals, String tasks, String casess) {
    	company.sendKeys(comp);
    	flag.sendKeys(flags);
    	closed.sendKeys(close);
    	deal.sendKeys(deals);
    	task.sendKeys(tasks);
    	cases.sendKeys(casess);
    	saveBtn.click();
    }
    
    public boolean clickCalls() {
    	callLabel.click();
    	return callInfo.isDisplayed();		
	}
}
