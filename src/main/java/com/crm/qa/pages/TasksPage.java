package com.crm.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.crm.qa.base.TestBase;

public class TasksPage extends TestBase {
	
	//Page Factory - OR:
    @FindBy(xpath="//td[contains(text(),'Tasks')]")
	WebElement tasksLabel;
    
    @FindBy(name="title")
    WebElement title;
    
    @FindBy(name="prospect_lookup")
    WebElement deal;
    
    @FindBy(name="client_lookup")
    WebElement keyCompany;
    
    @FindBy(name="task_type")
	WebElement type;
    
    @FindBy(xpath="//input[@type='submit' and @value='Save']")
	WebElement saveBtn;
    
    @FindBy(xpath="//input[@type='button' and @value='Show All']")
	WebElement showAll;
    
    @FindBy(xpath="//td[contains(text(),'Tasks-Showing all tasks')]")
	WebElement tasks;
    
    //Initializing the Page Object
    public TasksPage() {
		PageFactory.initElements(driver, this);
	}
    
    public boolean verifyTasksLabel() {
		return tasksLabel.isDisplayed();
	}
    
    public void createNewTasks(String titl, String deals, String keycompany, String typ) {
		title.sendKeys(titl);
		deal.sendKeys(deals);
		keyCompany.sendKeys(keycompany);
		type.sendKeys(typ);
		saveBtn.click();
	}
    
    public boolean clickTasks() {
    	tasksLabel.click();
    	showAll.click();
    	return tasks.isDisplayed();
		
	}
}
